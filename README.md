
# site-internet-env

**ES:** Proyecto Sitio internet para la iglesia Cristiana Evangélica Nueva vida

**FR :** Projet Site Internet pour l'église Chrétienne Evangélique Nouvelle vie

Auteur: **Claudio AGUEDO**

## Technologies utilisées
- Angular 10
- NPM 6.14.6
- Composer 1.10.9
- PHP 7.4.5
- Twig ^3.1
- Doctrine ORM ^2.7

## Prérequis
1. Avoir installé NPM et Angular
2. Avoir installé XAMPP serveur
3. Avoir installé PHP
4. Avoir installé Composer 

  
## Démarrage du site web sur un environnement local

**Attention:** la fonctionnalité d'envoie d'emails marche uniquement sur l'environnement productif (Hostinger)

#### 1. Création de la base de données et ses tables

1. Configurer la url de la base de données dans le fichier site-internet-env/api/.env

2. Ouvrir la terminal Windows dans le répertoire et se placer sur le chemin site-internet-env/api

3. Exécuter la commande: php bin/console doctrine:migrations:migrate

  

#### 2. Insertion de jeu de données de test sur la base de données

1. Lancer le service MySQL depuis l'interface de XAMPP

2. Ouvrir la page de PHPMyAdmin avec un navigateur depuis l'url: http://localhost/phpmyadmin/

3. Sélectionner la base de données du projet

4. Importer le script placé dans répertoire "site-internet-env/scripts"

  

#### 3. Construction du projet "private/private-access"

1. Ouvrir la terminal Windows dans le chemin "site-internet-env/private"
2. Exécuter la commande: npm install
3. Exécuter la commande: npm run build

#### 4. Construction du projet "api"

1. Ouvrir la terminal Windows dans le chemin "site-internet-env/api"
2. Exécuter la commande: composer update

#### 5. Construction du projet "site-internet-env"

1. Ouvrir la terminal Windows dans le chemin "site-internet-env"
2. Exécuter la commande: composer update

#### 6. Démarrage du projet site-internet-env avec XAMPP

1. Placer le dossier site-internet-env dans le répertoire C:\xampp\htdocs

2. Lancer le service Apache et le service MySQL depuis l'interface de XAMPP

#### Acceder au site web depuis un navigateur sur l'url: http://localhost/site-internet-env

####  Utilisateurs de test (jeu de données de test)
Rédacteur **/WRITER**
user:**redacteur**
pass:ef133a

Administrateur **/ADMIN**
user:**administrateur**
pass:01f275

## Annexe

#### Commandes PHP pour Doctrine

0. php bin/console doctrine:database:create

1. php bin/console make:entity

2. php bin/console make:migration

3. php bin/console doctrine:migrations:migrate

  

#### Commandes PHP pour Synfony

0. php bin/console make:controller

  

## References

https://rojas.io/building-a-jwt-authenticator-in-symfony-4/

https://symfony.com/doc/current/logging.html

https://www.cnil.fr/fr/cnil-direct/question/faut-il-declarer-un-site-web-la-cnil

https://www.zimrre.com/legal-app/legal.php