<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseController extends AbstractController
{
    protected  function responseOK($data = null, $message = null){
        return $this->createResponse($data, JsonResponse::HTTP_OK, $message);
    }

    protected  function createResponse($data, int $status, $message = null){
        $dataResponse = [];
        if($message === null)
            $dataResponse = ['data' => $data];
        else
            $dataResponse = ['data' => $data, 'message' => $message];
        return $this->json($dataResponse, $status);
    }

    protected  function responseCreated(){
        return $this->createResponse(["status" => "ok"], JsonResponse::HTTP_CREATED, "created");
    }

    protected  function responseUpdated(){
        return $this->createResponse(["status" => "ok"], JsonResponse::HTTP_OK, "updated");
    }

    protected  function responseDeleted(){
        return $this->createResponse(["status" => "ok"], JsonResponse::HTTP_OK , "deleted");
    }

    protected  function responseBadRequest($message = "bad request"){
        return $this->createResponse(["status" => "error"], JsonResponse::HTTP_BAD_REQUEST , $message);
    } 
}
