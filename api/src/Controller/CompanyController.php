<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Company;
use App\Repository\CompanyRepository;
use App\Utils\RequestUtils;

class CompanyController extends BaseController
{
    /**
     * @Route("/company/unique", methods={"GET"})
     */
    public function find(CompanyRepository $companyRepository)
    {
        $company = $companyRepository->findFirst();
        
        if($company === null) 
            return $this->responseOK([]);

        //Delete circular dependencies
        if(!empty($company->getEvents()))
            foreach ($company->getEvents() as &$event)
                $event->setCompany(null);

        if(!is_null($company->getOurHistory()))
            $company->getOurHistory()->setCompany(null);

        if(!is_null($company->getPrivacyPolicy()))
            $company->getPrivacyPolicy()->setCompany(null);

        return $this->responseOK($company);
    }

    /**
     * @Route("/company", methods={"PUT"})
     */
    public function update(Request $request, CompanyRepository $companyRepository)
    {
        $company = RequestUtils::deserialize($request, Company::class);
        $companyRepository->update($company);
        return $this->responseUpdated();
    }
}
