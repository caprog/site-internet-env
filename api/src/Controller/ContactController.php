<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Utils\RequestUtils;
use App\DTO\ContactDTO;
use App\Service\ContactService;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact", methods={"POST"})
     */
    public function index(Request $request, ContactService $contactService)
    {
        $contact = RequestUtils::deserialize($request, ContactDTO::class);
        $contactService->contact($contact);

        return $this->json([
            'message' => 'Contact successful'
        ]);
    }
}
