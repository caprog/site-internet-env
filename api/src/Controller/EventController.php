<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Event;
use App\Repository\EventRepository;
use App\Utils\RequestUtils;

class EventController extends AbstractController
{
    /**
     * @Route("/event", methods={"GET"})
     */
    public function findAll(EventRepository $eventRepository, Request $request)
    {
        $id = $request->query->get('id');
        if($id === null){
            $events = $eventRepository->findAll();
            return $this->createResponse($this->removeCircularReferences($events));
        }
        
        $event = $eventRepository->findById($id);
        
        if($event !== null)
            $event->setCompany(null);

        return $this->createResponse($event);
    }

    private function removeCircularReferences($events){
        foreach ($events as &$event)
            $event->setCompany(null);
        return $events;
    }

    /**
     * @template T of \Exception
     * @param T $exception
     * @return T
     */
    private function createResponse($data, $message = null){
        if($message === null)
            return $this->json(['data' => $data]);
        else
            return $this->json(['data' => $data, 'message' => $message]);
    }

    /**
     * @Route("/event", methods={"POST"})
     */
    public function create(Request $request, EventRepository $eventRepository)
    {
        $event = RequestUtils::deserialize($request, Event::class);
        $eventRepository->create($event);
        $event->setCompany(null);
        return $this->createResponse($event);
    }

    /**
     * @Route("/event", methods={"PUT"})
     */
    public function update(Request $request, EventRepository $eventRepository)
    {
        $event = RequestUtils::deserialize($request, Event::class);
        $saveEvent = $eventRepository->find($event->getId());
        $eventRepository->update($event);
        $saveEvent = $eventRepository->find($event->getId());
        $saveEvent->setCompany(null); // remove circular reference problem
        return $this->createResponse($saveEvent, "entity updated correctly");
    }

        /**
     * @Route("/event/{id}", methods={"DELETE"})
     */
    public function delete($id, EventRepository $eventRepository)
    {
        if(is_numeric($id)){
            $eventRepository->delete($id);
            return $this->json(['message' => 'Entity remove succefully']);
        }
        return new JsonResponse(['error' => 'The id is not numeric type'], JsonResponse::HTTP_CONFLICT);
    }
}
