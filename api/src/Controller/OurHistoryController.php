<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OurHistory;
use App\Repository\OurHistoryRepository;
use App\Utils\RequestUtils;

class OurHistoryController extends BaseController
{
    /**
     * @Route("/ourhistory/unique", methods={"GET"})
     */
    public function find(OurHistoryRepository $ourHistoryRepository)
    {
        $ourHistory = $ourHistoryRepository->findFirst();
        
        if($ourHistory === null) 
            return $this->responseOK([]);

        //Delete circular dependencies
        if(!is_null($ourHistory->getCompany()))
            $ourHistory->setCompany(null);

        return $this->responseOK($ourHistory);
    }

    /**
     * @Route("/ourhistory", methods={"PUT"})
     */
    public function update(Request $request, OurHistoryRepository $ourHistoryRepository)
    {
        $ourHistory = RequestUtils::deserialize($request, OurHistory::class);
        $ourHistoryRepository->update($ourHistory);
        return $this->responseUpdated();
    }
}
