<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PrivacyPolicy;
use App\Repository\PrivacyPolicyRepository;
use App\Utils\RequestUtils;

class PrivacyPolicyController extends BaseController
{
    /**
     * @Route("/privacypolicy/unique", methods={"GET"})
     */
    public function find(PrivacyPolicyRepository $privacyPolicyRepository)
    {
        $privacyPolicy = $privacyPolicyRepository->findFirst();
        
        if($privacyPolicy === null) 
            return $this->responseOK([]);

        //Delete circular dependencies
        if(!is_null($privacyPolicy->getCompany()))
            $privacyPolicy->setCompany(null);

        return $this->responseOK($privacyPolicy);
    }

    /**
     * @Route("/privacypolicy", methods={"PUT"})
     */
    public function update(Request $request, PrivacyPolicyRepository $privacyPolicyRepository)
    {
        $privacyPolicy = RequestUtils::deserialize($request, PrivacyPolicy::class);
        $privacyPolicyRepository->update($privacyPolicy);
        return $this->responseUpdated();
    }
}
