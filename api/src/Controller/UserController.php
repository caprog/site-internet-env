<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Utils\RequestUtils;
use Symfony\Component\Security\Core\Security;

class UserController extends BaseController
{
    /**
     * @Route("/user", methods={"GET"})
     */
    public function findAll(UserRepository $userRepository, Request $request)
    {
        $id = $request->query->get('id');
        if($id === null){
            $users = $this->removePassword($userRepository->findAll());
            return $this->responseOK($users);
        }
        $user = $userRepository->findById($id);
        $user->setPassword(null);
        return $this->responseOK($userRepository->findById($id));
    }

    public function removePassword($users){
        foreach($users as &$user)
            $user->setPassword(null);
        return $users;
    }

    /**
     * @Route("/user/session", methods={"GET"})
     */
    public function findUserSession(Security $security){
        return $this->responseOK($security->getUser());
    }

    /**
     * @Route("/user", methods={"POST"})
     */
    public function create(Request $request, UserService $userService)
    {
        $user = RequestUtils::deserialize($request, User::class);
        $userService->create($user);
        return $this->responseCreated();
    }

        /**
     * @Route("/auth", methods={"POST"})
     */
    public function auth(Request $request, UserService $userService)
    {
        $user = RequestUtils::deserialize($request, User::class);
        $token = $userService->login($user->getUsername(), $user->getPassword());
        return $this->responseOK(['token' => $token]);
    }

    /**
     * @Route("/user/resetpassword", methods={"PUT"})
     */
    public function resetPassword(UserService $userService, Request $request)
    {
        $user = RequestUtils::deserialize($request, User::class);
        $userService->resetPassword($user->getId());
        return $this->responseOK();
    }
    /**
     * @Route("/user", methods={"PUT"})
     */
    public function update(Request $request, UserRepository $userRepository)
    {
        $user = RequestUtils::deserialize($request, User::class);
        $userRepository->update($user);
        return $this->responseUpdated();
    }

        /**
     * @Route("/user/{id}", methods={"DELETE"})
     */
    public function delete($id, UserRepository $userRepository)
    {
        if(is_numeric($id)){
            $userRepository->delete($id);
            return $this->responseDeleted();
        }
        return $this->responseBadRequest("The id is not numeric");
    }
}
