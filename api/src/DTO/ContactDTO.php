<?php

namespace App\DTO;

/**
 */
class ContactDTO
{
    private $fullname;
    private $email;
    private $subject;
    private $message;

    public function getFullName(): ?string
    {
        return $this->fullname;
    }

    public function setFullName(string $fullname): ?self
    {
        $this->fullname = $fullname;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;
        return $this;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}
