<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $presentation;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $whatsapp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    
    /**
     * @ORM\Column(type="text")
     */
    private $gmapUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $availableWeekly;

    /**
     * @ORM\OneToOne(targetEntity=OurHistory::class, mappedBy="company", cascade={"persist", "remove"})
     */
    private $ourHistory;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="company")
     */
    private $events;

    /**
     * @ORM\OneToOne(targetEntity=PrivacyPolicy::class, mappedBy="company", cascade={"persist", "remove"})
     */
    private $privacyPolicy;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function setWhatsapp(string $whatsapp): self
    {
        $this->whatsapp = $whatsapp;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
    
    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    
    public function getGmapUrl(): ?string
    {
        return $this->gmapUrl;
    }

    public function setGmapUrl(string $gmapUrl): self
    {
        $this->gmapUrl = $gmapUrl;

        return $this;
    }

    public function getAvailableWeekly(): ?string
    {
        return $this->availableWeekly;
    }

    public function setAvailableWeekly(string $availableWeekly): self
    {
        $this->availableWeekly = $availableWeekly;

        return $this;
    }

    public function getOurHistory(): ?OurHistory
    {
        return $this->ourHistory;
    }

    public function setOurHistory(OurHistory $ourHistory): self
    {
        $this->ourHistory = $ourHistory;

        // set the owning side of the relation if necessary
        if ($ourHistory->getCompany() !== $this) {
            $ourHistory->setCompany($this);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setCompany($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getCompany() === $this) {
                $event->setCompany(null);
            }
        }

        return $this;
    }

    public function getPrivacyPolicy(): ?PrivacyPolicy
    {
        return $this->privacyPolicy;
    }

    public function setPrivacyPolicy(PrivacyPolicy $privacyPolicy): self
    {
        $this->privacyPolicy = $privacyPolicy;

        // set the owning side of the relation if necessary
        if ($privacyPolicy->getCompany() !== $this) {
            $privacyPolicy->setCompany($this);
        }

        return $this;
    }
}
