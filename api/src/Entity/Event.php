<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime" , nullable=true)
     */
    private $dateFrom;

    /**
     * @ORM\Column(type="datetime" , nullable=true)
     */
    private $dateTo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="boolean")
     */
    private $showCustomDateText;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $customDateText;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gmapLocation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgUrl;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCustomDateText(): ?string
    {
        return $this->customDateText;
    }

    public function setCustomDateText(string $customDateText): self
    {
        $this->customDateText = $customDateText;

        return $this;
    }
    
    public function getShowCustomDateText(): ?bool
    {
        return $this->showCustomDateText;
    }

    public function setShowCustomDateText(bool $showCustomDateText): self
    {
        $this->showCustomDateText = $showCustomDateText;

        return $this;
    }

    public function getDateFrom(): ?\DateTimeInterface
    {
        return $this->dateFrom;
    }

    public function setDateFrom(?\DateTimeInterface $dateFrom): self
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    public function getDateTo(): ?\DateTimeInterface
    {
        return $this->dateTo;
    }

    public function setDateTo(?\DateTimeInterface $dateTo): self
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getGmapLocation(): ?string
    {
        return $this->gmapLocation;
    }

    public function setGmapLocation(?string $gmapLocation): self
    {
        $this->gmapLocation = $gmapLocation;

        return $this;
    }
    public function getImgUrl(): ?string
    {
        return $this->imgUrl;
    }

    public function setImgUrl(?string $imgUrl): self
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
