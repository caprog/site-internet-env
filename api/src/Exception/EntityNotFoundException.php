<?php

namespace App\Exception;

use Exception;

class EntityNotFoundException extends Exception
{
    public function __construct($id){
        parent::__construct("Entity not found exception with id: " . $id);
    }
}