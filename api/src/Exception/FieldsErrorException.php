<?php

namespace App\Exception;

use Exception;

class FieldsErrorException extends Exception
{
    private $fieldsError = [];
    public const EMPTY_ERROR = "is empty"; 
    public const MIN_LENGTH_ERROR = "is too short"; 
    public const MAX_LENGTH_ERROR = "is too long"; 
    public const INVALID_ERROR = "is invalid"; 
    public function __construct(){
        parent::__construct("it exist fields with errors");
    }
    public function AddFieldError(string $fieldName, string $error){
        array_push($this->fieldsError, (object)["fieldName" => $fieldName, "error" => $error]);
    }

    public function hasErrors(){
        return count($this->fieldsError) > 0;
    }

    public function getFieldsErrors(){
        return count($this->fieldsError) > 0;
    }
}