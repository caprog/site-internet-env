<?php

namespace App\Exception;

use Exception;

class NullDTOException extends Exception
{
    public function __construct(){
        parent::__construct("Null dto exception");
    }
}