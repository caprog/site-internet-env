<?php

namespace App\Exception;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class UnauthorizedException extends Exception
{
    public function __construct(){
        parent::__construct("Unauthorized");
    }
}