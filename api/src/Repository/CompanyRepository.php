<?php

namespace App\Repository;

use App\Entity\Company;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }

    /**
     * Finds first.
     *
     * @return array The entities.
     */
    public function findFirst(): ?Company
    {
        $result = $this->findBy([]);
        return $result == null || empty($result) ? null : $result[0];
    }
    
    
    public function findById(int $id){
        return $this->find($id);
    } 

    public function update(Company $company){
        if($company->getId() == null)
            throw new NotIdInEntityException();
        $savedEntity = $this->find($company->getId());    
        $savedEntity = $this->updateEntity($company, $savedEntity);
        $this->getEntityManager()->persist($savedEntity);
        $this->getEntityManager()->flush();
    }

    private function updateEntity(Company $entityModified, Company $storedEntity){
        $storedEntity->setName($entityModified->getName());
        $storedEntity->setAddress($entityModified->getAddress());
        $storedEntity->setGmapUrl($entityModified->getGmapUrl());
        $storedEntity->setPresentation($entityModified->getPresentation());
        $storedEntity->setInstagram($entityModified->getInstagram());
        $storedEntity->setFacebook($entityModified->getFacebook());
        $storedEntity->setWhatsapp($entityModified->getWhatsapp());
        $storedEntity->setYouTube($entityModified->getYouTube());
        $storedEntity->setTelephone($entityModified->getTelephone());
        $storedEntity->setAvailableWeekly($entityModified->getAvailableWeekly());
        $storedEntity->setEmail($entityModified->getEmail());

        return $storedEntity; 
    }
}
