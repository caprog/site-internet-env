<?php

namespace App\Repository;

use App\Entity\Event;
use App\Exception\EntityNotFoundException;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\CompanyRepository;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    private $companyRepository;
    public function __construct(CompanyRepository $companyRepository, ManagerRegistry $registry)
    {
        
        $this->companyRepository = $companyRepository;
        parent::__construct($registry, Event::class);
    }

    public function findById(int $id){
        return $this->find($id);
    } 

    public function create(Event $event){
        $event->setCompany($this->companyRepository->findFirst());
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
    }

    public function update(Event $event){
        if($event->getId() == null)
            throw new NotIdInEntityException();
        $savedEvent = $this->find($event->getId());    
        $savedEvent = $this->updateEntity($event, $savedEvent);
        $this->getEntityManager()->persist($savedEvent);
        $this->getEntityManager()->flush();
    }

    private function updateEntity(Event $entityModified, Event $storedEntity){
        $storedEntity->setName($entityModified->getName());
        $storedEntity->setDescription($entityModified->getDescription());
        $storedEntity->setLocation($entityModified->getLocation());
        $storedEntity->setShowCustomDateText($entityModified->getShowCustomDateText());
        $storedEntity->setCustomDateText($entityModified->getCustomDateText());
        $storedEntity->setImgUrl($entityModified->getImgUrl());
        $storedEntity->setDateFrom($entityModified->getDateFrom());
        $storedEntity->setDateTo($entityModified->getDateTo());

        return $storedEntity; 
    }
    
    public function delete(int $id){
        $entity = $this->find($id);
        if($entity == null)
            throw new EntityNotFoundException($id);
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }
}
