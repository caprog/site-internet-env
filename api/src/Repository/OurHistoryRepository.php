<?php

namespace App\Repository;

use App\Entity\OurHistory;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OurHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method OurHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method OurHistory[]    findAll()
 * @method OurHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OurHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OurHistory::class);
    }
    /**
    * Finds first.
    *
    * @return array The entities.
    */
   public function findFirst(): ?OurHistory
   {
       $result = $this->findBy([]);
       return $result == null || empty($result) ? null : $result[0];
   }
   
   
   public function findById(int $id){
       return $this->find($id);
   } 

   public function update(OurHistory $ourHistory){
       if($ourHistory->getId() == null)
           throw new NotIdInEntityException();
       $savedEntity = $this->find($ourHistory->getId());    
       $savedEntity = $this->updateEntity($ourHistory, $savedEntity);
       $this->getEntityManager()->persist($savedEntity);
       $this->getEntityManager()->flush();
   }

   private function updateEntity(OurHistory $entityModified, OurHistory $storedEntity){
       $storedEntity->setCoverImageUrl($entityModified->getCoverImageUrl());
       $storedEntity->setContent($entityModified->getContent());

       return $storedEntity; 
   }
}
