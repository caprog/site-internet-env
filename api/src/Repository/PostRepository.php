<?php

namespace App\Repository;

use App\Entity\Post;
use App\Exception\EntityNotFoundException;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function create(Post $post){
        $this->getEntityManager()->persist($post);
        $this->getEntityManager()->flush();
    }

    public function update(Post $post){
        if($post->getId() == null)
            throw new NotIdInEntityException();
        $savedPost = $this->find($post->getId());    
        $savedPost = $this->updateEntity($post, $savedPost);
        $this->getEntityManager()->persist($savedPost);
        $this->getEntityManager()->flush();
    }

    private function updateEntity(Post $entityModified, Post $storedEntity){
        $storedEntity->setTitle($entityModified->getTitle());
        $storedEntity->setContent($entityModified->getContent());

        return $storedEntity; 
    }

    public function findById(int $id){
        return $this->find($id);
    }

    public function delete(int $id){
        $entity = $this->find($id);
        if($entity == null)
            throw new EntityNotFoundException($id);
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
