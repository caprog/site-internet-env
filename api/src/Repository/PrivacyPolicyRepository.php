<?php

namespace App\Repository;

use App\Entity\PrivacyPolicy;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrivacyPolicy|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrivacyPolicy|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrivacyPolicy[]    findAll()
 * @method PrivacyPolicy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrivacyPolicyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrivacyPolicy::class);
    }
    /**
    * Finds first.
    *
    * @return array The entities.
    */
    public function findFirst(): ?PrivacyPolicy
    {
        $result = $this->findBy([]);
        return $result == null || empty($result) ? null : $result[0];
    }
    
    
    public function findById(int $id){
        return $this->find($id);
    } 
 
    public function update(PrivacyPolicy $privacyPolicy){
        if($privacyPolicy->getId() == null)
            throw new NotIdInEntityException();
        $savedEntity = $this->find($privacyPolicy->getId());    
        $savedEntity = $this->updateEntity($privacyPolicy, $savedEntity);
        $this->getEntityManager()->persist($savedEntity);
        $this->getEntityManager()->flush();
    }
 
    private function updateEntity(PrivacyPolicy $entityModified, PrivacyPolicy $storedEntity){
        $storedEntity->setCoverImageUrl($entityModified->getCoverImageUrl());
        $storedEntity->setContent($entityModified->getContent());
 
        return $storedEntity; 
    }
}
