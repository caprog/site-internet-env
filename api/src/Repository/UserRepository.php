<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\EntityNotFoundException;
use App\Exception\NotIdInEntityException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findById(int $id){
        return $this->find($id);
    } 

    public function create(User $user){
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function update(User $user){
        if($user->getId() == null)
            throw new NotIdInEntityException();
        $savedUser = $this->find($user->getId());    
        $savedUser = $this->updateEntity($user, $savedUser);
        $this->getEntityManager()->persist($savedUser);
        $this->getEntityManager()->flush();
    }

    public function persist(User $user){
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    private function updateEntity(User $entityModified, User $storedEntity){
        $storedEntity->setName($entityModified->getName());
        $storedEntity->setLastname($entityModified->getLastname());
        $storedEntity->setEmail($entityModified->getEmail());
        $storedEntity->setProfile($entityModified->getProfile());

        return $storedEntity; 
    }
    
    public function delete(int $id){
        $entity = $this->find($id);
        if($entity == null)
            throw new EntityNotFoundException($id);
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }
}
