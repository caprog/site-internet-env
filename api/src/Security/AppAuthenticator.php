<?php
namespace App\Security;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class AppAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    private UserRepository $userRepository;
    
    private $paths = [
        "/user/{id}" => "ADMIN",
        "/user" => "ADMIN",
        "/user/session" => "ADMIN WRITER",
        "/user/resetpassword" => "ADMIN WRITER",
        "/company" => "WRITER",
        "/event" => "WRITER",
        "/event/{id}" => "WRITER",
        "/ourhistory/unique" => "WRITER",
        "/ourhistory" => "WRITER",
        "/ourhistory/{id}" => "WRITER",
        "/privacypolicy/unique" => "WRITER",
        "/privacypolicy" => "WRITER",
        "/privacypolicy/{id}" => "WRITER"
    ];

    private $route;
    private string $jwtsecret;

    // private UserRepository $userRepository;
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository, string $jwtsecret)
    {
        $this->jwtsecret = $jwtsecret;
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        $this->route = $request->getRequestUri();
        $exists =  $this->array_exist($this->paths, function ($path){
            if(strcmp($path, $this->route) === 0)
                return true;
            
            $exists = false;
            $sufix = strpos($path, '{id}');
            if($sufix){
                $prefix = substr($path, 0, $sufix);
                $pattern = '~' . $prefix . '[0-9]+' . '~';
                $exists = preg_match($pattern, $this->route);
            }
            return $exists === 1 ? true : false;
        });
        return $exists;
    }

    private function array_exist($array, $existsExpression){
        foreach ($array as $key => $val) {
            $exists = $existsExpression($key);
            if($exists)
                return $exists;
        }
        return false;
    }

    private function array_find($array, $existsExpression){
        foreach ($array as $key => $val) {
            $finded = $existsExpression($key, $val);
            if($finded !== null)
                return $finded;
        }
        return null;
    }

    private function getRolesUrl(){
        $roles = $this->array_find($this->paths, function($path, $val) {
            

            if($this->route === $path)
              return $val;
        
            $finded = null;
            $sufix = strpos($path, '{id}');
            if($sufix){
                $prefix = substr($path, 0, $sufix);
                $pattern = '~' . $prefix . '[0-9]+' . '~';
                $result = preg_match($pattern, $this->route);
                $finded = $result ? $val : null;
            }
            return $finded;
        });
        
        return explode(' ', $roles);
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        if(!$request->headers->has('X-Bearer-Token'))
            throw new AuthenticationException();
        return $request->headers->get('X-Bearer-Token');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $error = null;
        try
        {
            // If token exists
            $decodedJwt = JWT::decode($credentials, $this->jwtsecret, ['HS256']);
            $decodedJwt->user_id;
            $user = $this->userRepository->findById($decodedJwt->user_id);
            $user = clone $user;
            $user->setPassword(null);
            return $user;
        }
        catch(ExpiredException $e)
        {
            $error = "TOKEN_EXPIRED";
        }
        catch(SignatureInvalidException $e)
        {
            // In this case, you may also want to send an email to yourself with the JWT
            // If someone uses a JWT with an invalid signature, it could
            // be a hacking attempt.
            $error = "Attempting access invalid session.";
        }
        catch(\Exception $e)
        {
        // Use the default error message
            $error = $e->getMessage();
        }
        // Add log error
        return null;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $exist = false;
        $i = 0;
        $rolesCount = count($user->getRoles());
        while(!$exist && $i < $rolesCount){
            $exist = in_array($user->getRoles()[$i], $this->getRolesUrl());
            $i++;
        }
        return $exist;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
?>