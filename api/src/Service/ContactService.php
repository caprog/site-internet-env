<?php

namespace App\Service;

use App\DTO\ContactDTO;
use App\Exception\FieldsErrorException;
use App\Exception\NullDTOException;
use App\Utils\RequestUtils;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 */
class ContactService
{
    const EMAIL_REGEX = "<^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$>";
    private $params;
    private string $senderEmail;
    private string $receiverEmail;
    private EmailService $emailService;
    public function __construct(string $senderEmail, string $receiverEmail, EmailService $emailService)
    {
        $this->senderEmail = $senderEmail;
        $this->receiverEmail = $receiverEmail;
        $this->emailService = $emailService;
    }
    
    private function isValidEmail($email) {
        return (!preg_match(self::EMAIL_REGEX, $email)) ? FALSE : TRUE;
    }

    private function contactFromEmail(ContactDTO $contactDTO){
        $footer = "Sitio institucional:" . $contactDTO->getSubject();
        $lang = RequestUtils::getHttpAcceptLanguage();
        if(strcmp($lang, "fr") === 0) {
            $footer = "Site institutionnel:" . $contactDTO->getSubject();
        } 
        $this->emailService->sendEmail($this->receiverEmail, $contactDTO->getEmail(), $footer, $contactDTO->getMessage());
    }

    private function isMinLengthNotRespected(string $data, int $length){
        return $data == null ? true : strlen($data) < $length; 
    }

    private function isMaxLengthNotRespected(string $data, int $length){
        return $data == null ? false : strlen($data) > $length; 
    }

    public function contact(ContactDTO $contactDTO){
        if($contactDTO == null)
            throw new NullDTOException();

        $fieldsError = new FieldsErrorException();
        if(empty($contactDTO->getFullname()))
            $fieldsError->AddFieldError("fullname" , FieldsErrorException::EMPTY_ERROR);

        if(empty($contactDTO->getEmail()))
            $fieldsError.AddFieldError("email" , FieldsErrorException::EMPTY_ERROR);
        else if(!$this->isValidEmail($contactDTO->getEmail()))
            $fieldsError->AddFieldError("email" , FieldsErrorException::INVALID_ERROR);
        
        if(empty($contactDTO->getSubject()))
            $fieldsError->AddFieldError("subject" , FieldsErrorException::EMPTY_ERROR);
        
        if(empty($contactDTO->getMessage()))
            $fieldsError->AddFieldError("message" , FieldsErrorException::EMPTY_ERROR);
        
        if($this->isMinLengthNotRespected($contactDTO->getMessage(), 10))
            $fieldsError->AddFieldError("message" , FieldsErrorException::MIN_LENGTH_ERROR);
        
        if($this->isMaxLengthNotRespected($contactDTO->getMessage(), 350))
            $fieldsError->AddFieldError("message" , FieldsErrorException::MAX_LENGTH_ERROR);

        if($fieldsError->hasErrors())
            throw $fieldsError;

        $this->contactFromEmail($contactDTO);
    }
}
