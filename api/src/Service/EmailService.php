<?php

namespace App\Service;

/**
 */
class EmailService
{
    private $senderEmail;
    public function __construct($senderEmail){
        $this->senderEmail = $senderEmail;
    }
    
    public function sendEmail($to, $from, $subject, $message) {
        // $myfile = fopen("mail.txt", "w") or die("Unable to open file!");
        // $txt = $to . "\n" . $subject . "\n" . $message;
        // fwrite($myfile, $txt);
        // fclose($myfile);
        $headers = "From:" . $from . "\r\n".
        'Reply-To: ' . $from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    
        @mail($to, $subject, $message, $headers);
    }
}
