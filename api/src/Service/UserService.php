<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\UnauthorizedException;
use App\Repository\UserRepository;
use App\Utils\RequestUtils;
use App\Service\EmailService;
use Firebase\JWT\JWT;

/**
 */
class UserService
{
    private UserRepository $userRepository;
    private EmailService $emailService;
    private string $jwtsecret;
    private int $jwtexptime;
    public function __construct(string $senderEmail, EmailService $emailService, UserRepository $userRepository, string $jwtsecret, string $jwtexptime){
        $this->senderEmail = $senderEmail;
        $this->jwtsecret = $jwtsecret;
        $this->jwtexptime = (int) $jwtexptime;
        $this->emailService = $emailService;
        $this->userRepository = $userRepository;
    }
    
    public function create(User $user) {
        $password = $this->createRandomPassword();
        $passwordHashed = $this->hashPassword($password);
        $user->setPassword($passwordHashed);
        $this->userRepository->create($user);
        $lang = RequestUtils::getHttpAcceptLanguage();
        if(strcmp($lang, "fr") === 0) {
            $this->sendEmailNewAccountInFrench($user, $password);
        } else {
            $this->sendEmailNewAccountInSpanish($user, $password);
        }
    }

    private function createRandomPassword(){
        $password = sha1(microtime(true).mt_rand(10000,90000));
        return substr($password, 0, 6);
    }

    public function resetPassword(int $userId){
        $user = $this->userRepository->findById($userId);
        $password = $this->createRandomPassword();
        $passwordHashed = $this->hashPassword($password);
        $user->setPassword($passwordHashed);
        $this->userRepository->persist($user);

        $lang = RequestUtils::getHttpAcceptLanguage();
        if(strcmp($lang, "fr") === 0) {
            $this->sendEmailResetPasswordInFrench($user, $password);
        } else {
            $this->sendEmailResetPasswordInSpanish($user, $password);
        }
    }

    public function login(string $username, string $password){
        $user = $this->userRepository->findOneBy(['username' => $username]);
        if($user !== null && password_verify($password, $user->getPassword())){
            return $this->generateToken($user);
        };
        throw new UnauthorizedException();
    }

    public function generateToken(User $user){
        $expireTime = time() + $this->jwtexptime;
        $payload = array(
            "user_id" => $user->getId(),
            "exp" => $expireTime
        );

        // generate jwt
        $jwt = JWT::encode($payload, $this->jwtsecret);
        return $jwt;
    }

    private function sendEmailResetPasswordInFrench(User $user, string $password){
        $subject = "Mot de passe réinitialisé";
        $message = "Bonjour Madame, Monsieur\n\n";
        $message .= "Nous vous informons que votre mot de passe a été modifié.\n";
        $message .= "Voici votre nouveau mot de passe :" . $password . "\n\n";
        $message .= "Cordialement,\nL'équipe technique\nEglise Evangélique Nouvelle Vie";
        $this->emailService->sendEmail($user->getEmail(), $this->senderEmail, $subject, $message);
    }

    private function sendEmailResetPasswordInSpanish(User $user, string $password){
        $subject = "Contraseña reiniciada";
        $message = "Hola!\n\n";
        $message .= "Te informamos que tu contraseña fue reiniciada.";
        $message .= "He aquí tu nueva contraseña:" . $password . "\n\n";
        $message .= "Saludos,\nEl equipo técnico\nIglesia Evangélica Nueva Vida";
        $this->emailService->sendEmail($user->getEmail(), $this->senderEmail, $subject, $message);
    }

    private function sendEmailNewAccountInFrench(User $user, string $password){
        $subject = "Compte créé";
        $message = "Bonjour Madame, Monsieur\n\n";
        $message .= "Nous vous informons que votre compte a été créé.\n";
        $message .= "Nom d'utilisateur:" . $user->getUsername() . "\n";
        $message .= "Mot de passe:" . $password . "\n\n";
        $message .= "Cordialement,\nL'équipe technique\nEglise Evangélique Nouvelle Vie";
        $this->emailService->sendEmail($user->getEmail(), $this->senderEmail, $subject, $message);
    }

    private function sendEmailNewAccountInSpanish(User $user, string $password){
        $subject = "Cuenta creada";
        $message = "Hola!\n\n";
        $message .= "Te informamos que tu cuenta ha sido creada.\n";
        $message .= "Usuario:" . $user->getUsername(). "\n";
        $message .= "Contraseña:" . $password. "\n\n";
        $message .= "Saludos,\nEl equipo técnico\nIglesia Evangélica Nueva Vida";
        $this->emailService->sendEmail($user->getEmail(), $this->senderEmail, $subject, $message);
    }

    private function hashPassword($password){
        $password = password_hash($password, PASSWORD_DEFAULT);
        return $password;
    }
}
