<?php

namespace App\Utils;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

class RequestUtils
{
    public static function getHttpAcceptLanguage() {
        return strtolower(str_split($_SERVER['HTTP_ACCEPT_LANGUAGE'], 2)[0]);
    }
    public static function deserialize(Request $request, $clazz)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer(null, null, null, new ReflectionExtractor())];
        $serializer = new Serializer($normalizers, $encoders);
        
        $json = $request->getContent();
        return $serializer->deserialize($json, $clazz, 'json');
    }
}
