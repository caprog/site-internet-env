<?php
namespace App\Utils;
class I18nUtils {
    static function getCsvArray($path) {
        $array = array();
        $CSVfp = fopen($path, "r");
        if($CSVfp !== FALSE) {
            while(! feof($CSVfp)) {
                array_push($array, fgetcsv($CSVfp, 1000, ";"));
            }
        }
        fclose($CSVfp);
        return $array;
    }

    static function getI18nFromCsv($csvPath, $lang) {
        $csvArray = I18nUtils::getCsvArray($csvPath);
        $header = $csvArray[0];
        $termPos = 0;
        $colPos = array_search($lang, $header);
        $i18n = [];
        for ($i = 1; $i < count($csvArray) - 1 ; $i++) {
            $term = $csvArray[$i][$termPos];
            $i18n[$term] = $csvArray[$i][$colPos];
        }
        return $i18n;
    }
}
?>