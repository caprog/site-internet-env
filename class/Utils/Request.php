<?php
namespace App\Utils;
class Request {
    static $suffix = '/site-internet-env/';
    static function httpGet($url) {
        $response = file_get_contents(Request::url(Request::$suffix).$url);
        return json_decode($response);
    }
    static function url($suffix){
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $suffix
        );
    }
    static function getRequestUrl(){
        return str_replace(Request::$suffix, '', $_SERVER['REQUEST_URI']);
    }
    static function getRequestPage(){
        $url = Request::getRequestUrl();
        if (strpos($url, "?") === false)
            return $url;
        return substr($url, 0, strpos($url, "?"));
    }

    static function getRequestLang(){
        // default lang esp
        $lang = 'fr';
        $url = Request::getRequestUrl();
        if(strpos($url, '?lang=fr') === false)
            return $lang;
        return 'fr';
    }
}
?>