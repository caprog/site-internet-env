<?php
namespace App\Utils;

class TwigUtils {
    static function render($template, $dataArray) {
        try {
            // le dossier ou on trouve les templates
            $loader = new \Twig\Loader\FilesystemLoader('templates');
        
            // initialiser l'environement Twig
            $twig = new \Twig\Environment($loader);
        
            // load template
            $template = $twig->load($template);
        
            // set template variables
            // render template
            echo $template->render($dataArray);
        
        } catch (Exception $e) {
            die ('ERROR: ' . $e->getMessage());
        }
    }
}
?>