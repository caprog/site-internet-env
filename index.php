
<?php
    require_once 'vendor/autoload.php';
    use App\Utils\Request;
    use App\Utils\TwigUtils;
    use App\Utils\I18nUtils;
    function formatData($data) {
        if(empty($data->events) == false)
            foreach($data->events as $evt){
                $evt->date = date_format(date_create($evt->dateFrom), 'd-m-Y');
                $evt->hourFrom = date_format(date_create($evt->dateFrom), 'H:i');
                $evt->hourTo = date_format(date_create($evt->dateTo), 'H:i');
            }
        return $data;
    }

    // Select page to return
    $lang = Request::getRequestLang();
    $page = Request::getRequestPage();
    $page = $page == "" ? "home" : $page;
    $pages = array("home", "contact", "events", "our_history", "legal");
    if(in_array($page, $pages))
        TwigUtils::render($page.".html", [
            "page" => $page,
            "lang" => $lang,
            "company" => formatData(Request::httpGet('api/company/unique')->data),
            "translate" => I18nUtils::getI18nFromCsv('i18n.csv', $lang)
        ]);
    else
        TwigUtils::render("404.html", array());
?>