import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrudManagerComponent } from './crud-manager/crud-manager.component';
import { LoginComponent } from './login/login.component';
import { RefreshComponent } from './refresh/refresh.component';
import { AuthGuardService } from './service/security/auth-guard.service';
import { LoginGuardService } from './service/security/login-guard.service';
import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  { path:'login', component: LoginComponent, canActivate: [LoginGuardService]},
  { path:'refresh', component: RefreshComponent},
  { path: '', component: TemplateComponent, canActivate: [AuthGuardService],
    children: [
      { path: '**', component:CrudManagerComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
