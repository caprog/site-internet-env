import { Component } from '@angular/core';
import { I18nService } from './service/i18n/i18n.service';
import { LoaderService } from './service/loader/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'content-manager';
  loaded = false;
  public constructor(i18nService: I18nService, loaderService:LoaderService){
    loaderService.show();
    i18nService.on18nLoaded(() => {
      this.loaded = true;
      loaderService.hide();
    })
  }
}
