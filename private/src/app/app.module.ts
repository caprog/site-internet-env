import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrudComponent } from './crud/crud.component';
import { FormComponent } from './form/form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CrudManagerComponent } from './crud-manager/crud-manager.component';
import { TemplateComponent } from './template/template.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { CreateFormComponent } from './create-form/create-form.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { SharedModule } from './shared/shared.module';
import { HtmlEditorComponent } from './field/html-editor/html-editor.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDatetimeModule } from '@mat-datetimepicker/moment';
import { MatDatetimepickerModule, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { environment } from 'src/environments/environment';
import { RouterModule } from '@angular/router';
import { RefreshComponent } from './refresh/refresh.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './service/security/auth-guard.service';
import { JwkInterceptor } from './service/security/jwk-interceptor';
import { LoginGuardService } from './service/security/login-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    CrudComponent,
    FormComponent,
    TemplateComponent,
    UpdateFormComponent,
    CrudManagerComponent,
    CreateFormComponent,
    ConfirmDialogComponent,
    HtmlEditorComponent,
    RefreshComponent,
    LoginComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    // use this if you want to use native javascript dates and INTL API if available
    // MatNativeDatetimeModule,
    MatMomentDatetimeModule,
    MatDatetimepickerModule
  ],
  providers: [ 
    AuthGuardService,
    LoginGuardService,
    { provide: MAT_DATE_LOCALE, useValue: environment.defaultDateLang },
    {
      provide: MAT_DATETIME_FORMATS,
      useValue: {
        parse: {
          dateInput: "L",
          monthInput: "MMMM",
          timeInput: "LT",
          datetimeInput: "L LT"
        },
        display: {
          dateInput: "L",
          monthInput: "MMMM",
          datetimeInput: "L LT",
          timeInput: "LT",
          monthYearLabel: "MMM YYYY",
          dateA11yLabel: "LL",
          monthYearA11yLabel: "MMMM YYYY",
          popupHeaderDateLabel: "ddd, DD MMM"
        }
      }
    },
    [{ 
      provide: HTTP_INTERCEPTORS, 
      useClass: JwkInterceptor, 
      multi: true 
    }]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
