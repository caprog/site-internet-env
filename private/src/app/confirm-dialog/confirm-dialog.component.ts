import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { I18nService } from '../service/i18n/i18n.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent{
  constructor( 
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public modal: ConfirmDialogConfig,
    public i18nService: I18nService) { 
      if(this.modal.cancelButton === undefined)
        this.modal.cancelButton = this.i18nService.translate('confirm_dialog_cancel_button');
      if(this.modal.confirmButton === undefined)
        this.modal.confirmButton = this.i18nService.translate('confirm_dialog_confirm_button');
      if(this.modal.message === undefined)
        this.modal.message = this.i18nService.translate('confirm_dialog_message');
      if(this.modal.title === undefined)
        this.modal.title = this.i18nService.translate('confirm_dialog_title');
  }

  submit(){
    if(this.modal.actionCallback)
      this.modal.actionCallback(this.dialogRef);
    else
      this.close();
  }

  close(){
    this.dialogRef.close();
  }
}

export class ConfirmDialogConfig {
  public title?: string;
  public message: string;
  public confirmButton?: string;
  public cancelButton?: string;
  public actionCallback: (dialogRef: MatDialogRef<ConfirmDialogComponent>) => void;
}