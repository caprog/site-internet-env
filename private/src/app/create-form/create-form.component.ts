import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CrudDef } from '../crud/crud-def';
import { FormUtils } from '../form/form-utils';
import { FormService } from '../service/form/form.service';
import { ApiResponse } from '../service/http/ApiResponse';
import { HttpService } from '../service/http/http.service';
import { I18nService } from '../service/i18n/i18n.service';
import { LoaderService } from '../service/loader/loader.service';
import { NotificationService } from '../service/notification/notification.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit{

  @Input()
  public crudDef: CrudDef;
  private submitting: boolean;
  createForm: FormGroup;
 
  constructor(
      private formBuilder: FormBuilder,
      private i18nService: I18nService,
      private httpService: HttpService,
      private formService: FormService,
      private notificationService: NotificationService,
      private loaderService: LoaderService) {}

  ngOnInit(){
    this.submitting = false;
    this.createForm =  this.formService.buildForm(this.crudDef.forms.create.fields);
  }

  submit(): void{
    if(this.submitting)
      return;

    this.submitting = true;
    if(this.createForm.valid){
      this.loaderService.show(this.i18nService.translate('crud_create_form_saving_data'));
      this.httpService
        .post(this.crudDef.apiUrl, FormUtils.getDataControls<any>(this.createForm.controls))
        .subscribe(
          (r: ApiResponse) => {
            this.loaderService.hide();
            this.notificationService.showSuccessMessage(this.translate('crud_create_form_notify_success_message'));
            this.submitting = false;
            this.createForm.reset();
          },
          err => {
            this.notificationService.showServerError();
            this.loaderService.hide();
            this.submitting = false;
            console.error(err);
          });
    }
  }

  translate(termKey: string): string{
    return this.i18nService.translate(termKey);
  }
}
