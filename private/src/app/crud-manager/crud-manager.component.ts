import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudDef } from '../crud/crud-def';
import { CrudDefService } from '../service/crud-def/crud-def.service';


@Component({
  selector: 'app-crud-manager',
  templateUrl: './crud-manager.component.html',
  styleUrls: ['./crud-manager.component.scss']
})
export class CrudManagerComponent implements OnInit {
  public crudDef: CrudDef;
  constructor(private router: Router, private crudDefService: CrudDefService) {
    // router.events.subscribe((val) => this.ngOnInit());
  }

  ngOnInit(): void {
    this.crudDef = undefined;
    const crudDefName = this.parseUrl(this.router.url);
    this.crudDefService.findCrudDefByName(crudDefName).subscribe((crudDef: CrudDef) => {
      this.crudDef = crudDef;
    })
  }

  private parseUrl(url: string) {
    return url.substring(1, url.length).split('/').join('_');
  }
}
