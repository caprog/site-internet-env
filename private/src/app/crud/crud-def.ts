import { FormDef } from '../form/form-def';
import { GridDef } from '../grid/grid-def';

export class CrudDef {
    key:string;
    title?: string;
    titleTermKey: string;
    grid?: GridDef;
    showTabs?: boolean;
    securityProfile?: string;
    selectedTab?: 'update' | 'search' | 'create';
    forms?: {
        create?: FormDef,
        update?: FormDef,
        read?: FormDef,
        filter?: FormDef
    }
    apiUrl: string;
}
