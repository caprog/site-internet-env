import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { ConfirmDialogConfig, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { FormatDef, GridAction } from '../grid/grid-def';
import { ApiResponse } from '../service/http/ApiResponse';
import { HttpRequest, HttpService } from '../service/http/http.service';
import { I18nService } from '../service/i18n/i18n.service';
import { LoaderService } from '../service/loader/loader.service';
import { NotificationService } from '../service/notification/notification.service';
import { TemplateService } from '../service/template/template.service';
import * as moment from 'moment';
import { CrudDef } from './crud-def';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit, AfterViewInit {
  // For table
  @Input()
  crudDef: CrudDef;
  dataSource: MatTableDataSource<any> = new MatTableDataSource([]);;
  @ViewChild(MatTabGroup) tabs: MatTabGroup;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  headers: string[];

  // Search Form
  searchForm: FormGroup;
  dataList: any[];

  // Update Form
  idData:number;

  constructor(private i18nService: I18nService, 
              private httpService: HttpService, 
              private formBuilder: FormBuilder,
              private dialog: MatDialog,
              private changeDetectorRefs: ChangeDetectorRef,
              private notificationService: NotificationService,
              private loaderService: LoaderService,
              private templateService: TemplateService) { }


  ngAfterViewInit(): void {
    if(!this.isOnlyUpdatePage())
      this.translatePaginator();
  }

  ngOnInit(): void {
    this.templateService.changeTitle(this.translate(this.crudDef.titleTermKey));
    if(this.isOnlyUpdatePage())
      return;
    
    this.httpService.subscribe({
      executeAfterSuccess: (httpRequest:HttpRequest) => {
        if(httpRequest.url == this.crudDef.apiUrl && httpRequest.method == 'post') {
          this.tabs.selectedIndex = 0; // redirect to grid;
          this.loadGrid();      
        }
      }
    })
    this.dataSource.paginator = this.paginator;
    this.headers = ['_actions'].concat(this.crudDef.grid.displayedColumns);
    this.loadGrid();
    this.searchForm = this.formBuilder.group({
      term: ['']
    });
  }

  translatePaginator() {
    this.paginator._intl.itemsPerPageLabel = this.translate('crud_grid_paginator_per_page');
    this.paginator._intl.firstPageLabel = this.translate('crud_grid_paginator_firstpage');
    this.paginator._intl.lastPageLabel = this.translate('crud_grid_paginator_lastpage');
    this.paginator._intl.nextPageLabel = this.translate('crud_grid_paginator_nextpage');
    this.paginator._intl.previousPageLabel = this.translate('crud_grid_paginator_previuspage');
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      const start = page * pageSize + 1;
      const end = (page + 1) * pageSize;
      return `${start} - ${end} ${this.translate('crud_grid_paginator_of')} ${length}`;
    };
  }

  isOnlyUpdatePage(){
    return this.crudDef.grid === undefined && 
            this.crudDef.forms.create === undefined &&
              this.crudDef.forms.read === undefined;
  }

  private updateDatasource(data: any []){
    this.dataSource.data = data;
    this.dataSource.paginator = this.paginator;
    this.changeDetectorRefs.detectChanges();
  }

  private loadGrid(){
    this.loaderService.show(this.translate('crud_grid_load_grid'));
    this.httpService.get(this.crudDef.apiUrl)
      .subscribe((r: ApiResponse) => {
        this.dataList = r.data;
        this.submitSearch();
      });
  }

  getElementValue(el, key, format): any{
    let value = el[key];
    if(value !== null && 
        value !== undefined && 
          format === FormatDef.DATETIME)
      return moment(value).format(environment.datetimeFormat);
      
    return value;
  }

  onTabChanged($event) {
    if($event.index != 2)
      this.idData = undefined; // Close tab update
  }

  translate(termKey: string):string{
    return this.i18nService.translate(termKey);
  }
  // Search Form
  submitSearch(){
    if(this.searchForm.valid){
      const term = this.searchForm.controls.term.value;
      const data = this.searchTermInData(this.dataList, term);
      this.updateDatasource(data);
      this.loaderService.hide();
    }
  }

  searchTermInData(data: any[], term: string) {
    if(term == '' || term == undefined)
      return data;
    else 
      return data.filter(d => {
        return Object.getOwnPropertyNames(d).find(prop => {
          const value = d[prop];
          if(typeof value === 'string')
            return value.toLowerCase().includes(term.toLowerCase());
          else 
            return value === term;
        });
      });
  }

  getActionsByElement(el: any){
    const GRID_ACTIONS = [
      {
        key: 'delete',
        actionNameTermKey: 'crud_grid_delete_action',
        icon: 'delete'
      },
      {
        key: 'update',
        actionNameTermKey: 'crud_grid_update_action',
        icon: 'edit'
      },
    ];

    return GRID_ACTIONS;
  }
  
  onSubmitAction(action:any, element: any){
    if(action === 'update'){
      this.idData = element.id;
      this.tabs.selectedIndex = 2; // Select update tab;
    }else if(action === 'delete')
      this.callDeleteOperation(element.id);
  }

  executeActionWithDialog(action:GridAction, id:number){
    const modalConfig: ConfirmDialogConfig = {
      message : this.i18nService.translate(action.dialog.messageTermKey),
      actionCallback: (dialogRef: MatDialogRef<ConfirmDialogComponent>) => {
        this.loaderService.show(this.translate('confirm_dialog_executing'));
        this.httpService.put(action.apiUrl, {id})
          .subscribe(
            (r:ApiResponse) => {
            this.loaderService.show(this.translate('confirm_dialog_success'));
              setTimeout(() => {
                this.loadGrid();
              }, 2000)
            },
            err => this.notificationService.showServerError()
            );
        dialogRef.close();
      }
    }
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: modalConfig
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  
  executeAction(action: GridAction, id:number) {
    if(action.dialog) 
      this.executeActionWithDialog(action, id);
  }
  
  callDeleteOperation(id: any) {
    const modalConfig: ConfirmDialogConfig = {
      message : this.i18nService.translate('crud_grid_delete_confirm_message'),
      actionCallback: (dialogRef: MatDialogRef<ConfirmDialogComponent>) => {
        this.loaderService.show(this.translate('crud_grid_delete_deleting_data'));
        this.httpService.delete(this.crudDef.apiUrl, id)
          .subscribe((r:ApiResponse) => {
            this.loaderService.show(this.translate('crud_grid_delete_success'));
            setTimeout(() => {
              this.loadGrid();
            }, 2000)
          });
        dialogRef.close();
      }
    }
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: modalConfig
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}



