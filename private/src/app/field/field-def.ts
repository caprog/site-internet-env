type ControlType = "email" | 
                    "textbox" |
                    "password" |
                    "number" |
                    "html_editor" |
                    "datetime" |
                    "select" |
                    "checkbox";

export enum ControlTypeEnum {
    email = 'email',
    textbox = 'textbox',
    password = 'password',
    number = 'number',
    html_ditor = 'html_editor',
    datetime = 'datetime',
    select = 'select',
    checkbox = 'checkbox'
}

export class FieldDef {
    key: string;
    label?: string;
    labelTermKey?: string;
    controlType: ControlType;
    value?: any;
    required?: boolean;
    disabled?: boolean;
    maxLength?: any;
    minLength?: any;
    minValue?: any;
    maxValue?: any;
    length?: any;
    options?: any;

}
