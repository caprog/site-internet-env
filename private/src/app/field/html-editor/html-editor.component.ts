import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
export class HtmlEditorConfig {
  public tabEditorLabel: string;
  public tabCodeLabel: string;
}
@Component({
  selector: 'app-html-editor',
  templateUrl: './html-editor.component.html',
  styleUrls: ['./html-editor.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HtmlEditorComponent),
      multi: true
    }
  ]
})
export class HtmlEditorComponent implements ControlValueAccessor{
  normalize(evt): void {
    const text = evt.editor.data.get();
    evt.editor.data.set(text.replace(/&nbsp;/g,' '));
  }

  public Editor = ClassicEditor;
  
  @Input()
  public config: HtmlEditorConfig = {
    tabEditorLabel: '#editor',
    tabCodeLabel: '</>code'
  };

  @Input()
  label:string;

  _value:any = '';
  get value(): any { return this._value; }
  set value(v: any) {
    if (v && v !== this._value) {
      this._value = v;
      this.propagateChange(v);
    }
  }

  writeValue(obj: any): void {
    if(obj !== undefined)
      this.value = obj;
  }

  propagateChange = (_: any) => {};
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }
}
