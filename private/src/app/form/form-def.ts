import { FieldDef } from '../field/field-def';

export class FormDef {
    key?: string;
    title?: string;
    titleTermKey?: string;
    fields?: FieldDef[];
    apiWs?: string;
    apiGet?: string;
}
