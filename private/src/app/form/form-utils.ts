import { AbstractControl } from '@angular/forms';

export class FormUtils {
    public static getDataControls<T>(controls: any):T{
        const data:any = {};
        Object.getOwnPropertyNames(controls)
            .forEach(ctrlName => data[ctrlName] = controls[ctrlName].value);
        return data;
    }
}