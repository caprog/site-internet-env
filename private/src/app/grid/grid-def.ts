export enum FormatDef{
    DATETIME = 'datetime'
}
export class ColumnDef {
    id?: boolean;
    columnDef!: string;
    columnNameTermKey!: string;
    format?: 'datetime';
}

export class Dialog{
    messageTermKey:string;
}

export class GridAction{
    icon: string;
    dialog?: Dialog;
    apiUrl: string;
}

export class GridDef {
    columnsDef?: ColumnDef[];
    displayedColumns?: string[];
    actions?: GridAction[]
}
