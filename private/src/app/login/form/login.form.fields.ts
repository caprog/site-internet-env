import { FieldDef } from 'src/app/field/field-def';

export const LOGIN_FORM_FIELDS_DEF: FieldDef[] = [
    {
      key: 'username',
      labelTermKey : 'login_form_field_username',
      required: true,
      controlType: 'textbox'
    },
    {
      key: 'password',
      labelTermKey : 'login_form_field_password',
      required: true,
      controlType: 'password'
    },
    {
      key: 'remember',
      labelTermKey : 'login_form_field_remember',
      controlType: 'checkbox'
    }
];
