import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormUtils } from '../form/form-utils';
import { FormService } from '../service/form/form.service';
import { I18nService } from '../service/i18n/i18n.service';
import { LoaderService } from '../service/loader/loader.service';
import { NotificationService } from '../service/notification/notification.service';
import { AuthService } from '../service/security/auth.service';
import { LOGIN_FORM_FIELDS_DEF } from './form/login.form.fields';

export const LOGIN_NAME = 'login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  public form: FormGroup;
  private submitting;
  constructor(private i18nService: I18nService, 
                private formService: FormService,
                  private notificationService: NotificationService,
                    private loaderService: LoaderService,
                      private authService: AuthService,
                        private router: Router) {}
  
  ngOnInit(): void {
    this.form = this.formService.buildForm(LOGIN_FORM_FIELDS_DEF);
    this.submitting = false;
  }

  submit(): void{
    if(this.submitting)
      return;

    this.submitting = true;
    if(this.form.valid){
      this.loaderService.show(this.i18nService.translate('login_form_submitting'));
      const user = FormUtils.getDataControls<any>(this.form.controls);
      this.authService.login(user.username, user.password)
        .subscribe(
          (ok) => {
            this.loaderService.hide();
            this.notificationService.showSuccessMessage(this.translate('login_form_user_connected'));
            this.submitting = false;
            this.router.navigate(['/']);
          },
          err => {
            this.loaderService.hide();
            this.submitting = false;
            if(err.status === 401)
              this.notificationService.showErrorMessage(this.translate('login_form_unauthorized'));
            else
              this.notificationService.showServerError();
          });
    }
  }

  translate(term: string){
    return this.i18nService.translate(term);
  }
}
