import { ComponentDef } from '../../../fwk/core/model/component-def/component-def';
import { LOGIN_FORM_FIELDS_DEF } from './form/login.form.fields';
import { environment } from '../../../../environments/environment';

export const LOGIN_DEF: ComponentDef = {
    name: 'login',
    security: {
        createAccess: '',
        updateAccess: '',
        readAccess: '',
        deleteAccess: '',
    },
    navigation: {
        id: 'login-nav',
        translateKey : 'login_nav_title',
        url: 'auth/login',
        showMenu: false
    },
    formsList: [
        {
            key: 'login_form',
            fields: LOGIN_FORM_FIELDS_DEF
        }
    ],
    ws: {
        key: 'login_auth_ws',
        url: environment.AUTH_URL
    }
};
