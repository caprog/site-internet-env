import { TestBed } from '@angular/core/testing';

import { CrudDefService } from './crud-def.service';

describe('CrudDefService', () => {
  let service: CrudDefService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudDefService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
