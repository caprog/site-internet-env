import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CrudDef } from 'src/app/crud/crud-def';
import { CRUDS_DEF } from './cruds-def';

@Injectable({
  providedIn: 'root'
})
export class CrudDefService {
  private crudsDef:CrudDef[] = CRUDS_DEF
  constructor() { }

  public findCrudDefByName(name: string): Observable<CrudDef>{
    return new Observable((observer) => {
      const crudDef = this.crudsDef.find(cd => cd.key.toLowerCase() === name.toLowerCase());
      observer.next(crudDef);
    });
  }

  public findAllCrudDef(): Observable<CrudDef[]>{
    return new Observable((observer) => {
      observer.next(this.crudsDef);
    });
  }
}
