import { CrudDef } from 'src/app/crud/crud-def';
import { environment } from 'src/environments/environment';
import { COMPANY_UPDATE_FORM_FIELDS_DEF } from './form/company.update.form.fields.def';

// Definicion de un template crud(Create,Read,Update and Delete)
export const COMPANY_DEF: CrudDef = { 
    key: 'company',
    titleTermKey: 'company_title',
    showTabs: false,
    securityProfile: 'WRITER',
    selectedTab: "update",
    forms: {
        update : {
            apiGet: environment.apiHost + '/company/unique',
            fields: COMPANY_UPDATE_FORM_FIELDS_DEF
        }
    },
    apiUrl: environment.apiHost + '/company'
};
