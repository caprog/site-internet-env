import { FieldDef } from "src/app/field/field-def";

export const COMPANY_UPDATE_FORM_FIELDS_DEF: FieldDef [] = [
   {
      key: 'name',
      labelTermKey: 'company_update_form_name_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 100
   },
   {
      key: 'presentation',
      labelTermKey: 'company_update_form_presentation_field_label',
      controlType: 'html_editor',
      required: true
   },
   {
      key: 'address',
      labelTermKey: 'company_update_form_address_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 150
   },
   {
      key: 'gmapUrl',
      labelTermKey: 'company_update_form_gmapurl_field_label',
      controlType: 'textbox',
      required: true,
   },
   {
      key: 'facebook',
      labelTermKey: 'company_update_form_facebook_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 120
   },
   {
      key: 'instagram',
      labelTermKey: 'company_update_form_instagram_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 120
   },
   {
      key: 'whatsapp',
      labelTermKey: 'company_update_form_whatsapp_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 120
   },
   {
      key: 'youtube',
      labelTermKey: 'company_update_form_youtube_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 120
   },
   {
      key: 'telephone',
      labelTermKey: 'company_update_form_telephone_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 120
   },
   {
      key: 'email',
      labelTermKey: 'company_update_form_email_field_label',
      controlType: 'email',
      required: true,
      maxLength: 255
   },
   {
      key: 'availableWeekly',
      labelTermKey: 'company_update_form_availableweekly_field_label',
      controlType: 'textbox',
      required: true,
      maxLength: 255
   }
 ];
 