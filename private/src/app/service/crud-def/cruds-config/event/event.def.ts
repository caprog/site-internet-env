import { EVENT_CREATE_FORM_FIELDS_DEF } from './form/event.create.form.fields.def';
import { EVENT_UPDATE_FORM_FIELDS_DEF } from './form/event.update.form.fields.def';

import { EVENT_GRID_DEF } from './grid/event.grid.def';
import { CrudDef } from 'src/app/crud/crud-def';
import { environment } from 'src/environments/environment';

// Definicion de un template crud(Create,Read,Update and Delete)
export const EVENT_DEF: CrudDef = { 
    key: 'event',
    titleTermKey: 'event_title',
    grid: EVENT_GRID_DEF,
    securityProfile: 'WRITER',
    forms: {
        create: {
            fields: EVENT_CREATE_FORM_FIELDS_DEF,
        },
        update : {
            fields: EVENT_UPDATE_FORM_FIELDS_DEF
        }
    },
    apiUrl: environment.apiHost + '/event'
};
