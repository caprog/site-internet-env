import { FieldDef } from "src/app/field/field-def";

export const EVENT_CREATE_FORM_FIELDS_DEF: FieldDef [] = [
  {
     key: 'name',
     label: 'Nom',
     labelTermKey: 'event_create_form_name_field_label',
     controlType: 'textbox',
     maxLength: 150,
     required: true
  },
  {
     key: 'description',
     label: 'Description',
     labelTermKey: 'event_create_form_description_field_label',
     controlType: 'textbox',
     maxLength: 255,
     required: true
  },
  {
     key: 'location',
     label: 'Location',
     labelTermKey: 'event_create_form_location_field_label',
     controlType: 'textbox',
     maxLength: 255,
     required: true
  },
  {
     key: 'imgUrl',
     labelTermKey: 'event_create_form_imgurl_field_label',
     controlType: 'textbox',
     maxLength: 255,
     required: true
  },
  {
   key: 'showCustomDateText',
   labelTermKey: 'event_create_form_showcustomdatetext_field_label',
   controlType: 'checkbox'
  },
  {
   key: 'customDateText',
   labelTermKey: 'event_create_form_customdatetext_field_label',
   controlType: 'html_editor'
  },
  {
     key: 'dateFrom',
     label: 'Date de depart',
     labelTermKey: 'event_create_form_datefrom_field_label',
     controlType: 'datetime'
  },
  {
     key: 'dateTo',
     label: 'Date de fin',
     labelTermKey: 'event_create_form_dateto_field_label',
     controlType: 'datetime'
  }
];
