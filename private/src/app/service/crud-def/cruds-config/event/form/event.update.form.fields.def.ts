import { FieldDef } from "src/app/field/field-def";

export const EVENT_UPDATE_FORM_FIELDS_DEF: FieldDef [] = [
  {
     key: 'name',
     label: 'Nom',
     labelTermKey: 'event_update_form_name_field_label',
     controlType: 'textbox',
     maxLength: 150
  },
  {
     key: 'description',
     label: 'Description',
     labelTermKey: 'event_update_form_description_field_label',
     controlType: 'textbox',
     maxLength: 255
  },
  {
     key: 'location',
     label: 'Location',
     labelTermKey: 'event_update_form_location_field_label',
     controlType: 'textbox',
     maxLength: 255
  },
  {
     key: 'imgUrl',
     labelTermKey: 'event_update_form_imgurl_field_label',
     controlType: 'textbox',
     maxLength: 255
  },
  {
   key: 'showCustomDateText',
   labelTermKey: 'event_update_form_showcustomdatetext_field_label',
   controlType: 'checkbox'
  },
  {
   key: 'customDateText',
   labelTermKey: 'event_update_form_customdatetext_field_label',
   controlType: 'html_editor'
  },
  {
     key: 'dateFrom',
     label: 'Date de depart',
     labelTermKey: 'event_update_form_datefrom_field_label',
     controlType: 'datetime'
  },
  {
     key: 'dateTo',
     label: 'Date de fin',
     labelTermKey: 'event_update_form_dateto_field_label',
     controlType: 'datetime'
  }
];
