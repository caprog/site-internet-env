import { GridDef } from 'src/app/grid/grid-def';

export const EVENT_GRID_DEF: GridDef = {
  columnsDef: [
    {
       id: true,
       columnDef: 'id',
       columnNameTermKey: 'event_grid_column_id'
    },
    {
       columnDef: 'name',
       columnNameTermKey: 'event_grid_column_name'
    },
    {
       columnDef: 'description',
       columnNameTermKey: 'event_grid_column_description'
    },
    {
       columnDef: 'location',
       columnNameTermKey: 'event_grid_column_location'
    },
    {
       columnDef: 'dateFrom',
       columnNameTermKey: 'event_grid_column_datefrom',
       format:'datetime'
    },
    {
       columnDef: 'dateTo',
       columnNameTermKey: 'event_grid_column_dateto',
       format:'datetime'
    }
  ],
  displayedColumns: [
      'name',
      'description',
      'location',
      'dateFrom',
      'dateTo'
  ]
};
