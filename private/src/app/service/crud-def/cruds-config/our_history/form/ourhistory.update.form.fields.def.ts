import { FieldDef } from "src/app/field/field-def";

export const OURHISTORY_UPDATE_FORM_FIELDS_DEF: FieldDef[] = [
   {
      key: 'coverImageUrl',
      labelTermKey: 'ourhistory_update_form_coverimageurl_field_label',
      controlType: 'textbox',
      required: true
   },
   {
      key: 'content',
      labelTermKey: 'ourhistory_update_form_content_field_label',
      controlType: 'html_editor',
      required: true
   }
 ];
 