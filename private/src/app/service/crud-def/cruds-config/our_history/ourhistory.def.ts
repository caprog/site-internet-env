import { CrudDef } from 'src/app/crud/crud-def';
import { environment } from 'src/environments/environment';
import { OURHISTORY_UPDATE_FORM_FIELDS_DEF } from './form/ourhistory.update.form.fields.def';

// Definicion de un template crud(Create,Read,Update and Delete)
export const OURHISTORY_DEF: CrudDef = { 
    key: 'ourhistory',
    titleTermKey: 'ourhistory_title',
    showTabs: false,
    selectedTab: "update",
    securityProfile: 'WRITER',
    forms: {
        update : {
            apiGet: environment.apiHost + '/ourhistory/unique',
            fields: OURHISTORY_UPDATE_FORM_FIELDS_DEF
        }
    },
    apiUrl: environment.apiHost + '/ourhistory'
};
