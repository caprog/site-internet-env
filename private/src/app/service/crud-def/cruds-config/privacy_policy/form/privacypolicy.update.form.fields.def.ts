import { FieldDef } from "src/app/field/field-def";

export const PRIVACYPOLICY_UPDATE_FORM_FIELDS_DEF: FieldDef[] = [
   {
      key: 'coverImageUrl',
      labelTermKey: 'privacypolicy_update_form_coverimageurl_field_label',
      controlType: 'textbox',
      required: true
   },
   {
      key: 'content',
      labelTermKey: 'privacypolicy_update_form_content_field_label',
      controlType: 'html_editor',
      required: true
   }
 ];
 