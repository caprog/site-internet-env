import { CrudDef } from 'src/app/crud/crud-def';
import { environment } from 'src/environments/environment';
import { PRIVACYPOLICY_UPDATE_FORM_FIELDS_DEF } from './form/privacypolicy.update.form.fields.def';

// Definicion de un template crud(Create,Read,Update and Delete)
export const PRIVACYPOLICY_DEF: CrudDef = { 
    key: 'privacypolicy',
    titleTermKey: 'privacypolicy_title',
    showTabs: false,
    selectedTab: "update",
    securityProfile: 'WRITER',
    forms: {
        update : {
            apiGet: environment.apiHost + '/privacypolicy/unique',
            fields: PRIVACYPOLICY_UPDATE_FORM_FIELDS_DEF
        }
    },
    apiUrl: environment.apiHost + '/privacypolicy'
};
