import { FieldDef } from "src/app/field/field-def";

export const USER_CREATE_FORM_FIELDS_DEF:FieldDef[] = [
  {
     key: 'username',
     labelTermKey: 'user_create_form_username_field_label',
     controlType: 'textbox',
     required: true,
     maxLength: 20
  },
  {
     key: 'name',
     labelTermKey: 'user_create_form_name_field_label',
     controlType: 'textbox',
     required: true,
     maxLength: 80
  },
  {
     key: 'lastname',
     labelTermKey: 'user_create_form_lastname_field_label',
     controlType: 'textbox',
     required: true,
     maxLength: 80
  },
  {
     key: 'email',
     labelTermKey: 'user_create_form_email_field_label',
     controlType: 'email',
     required: true,
     maxLength: 255
  },
  {
     key: 'profile',
     labelTermKey: 'user_create_form_profile_field_label',
     controlType: 'select',
     required: true,
     options: {
        data: ['ADMIN', 'WRITER']
     }
  }
];
