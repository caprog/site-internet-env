import { GridDef } from 'src/app/grid/grid-def';
import { environment } from 'src/environments/environment';

export const USER_GRID_DEF: GridDef = {
  columnsDef: [
    {
       id: true,
       columnDef: 'id',
       columnNameTermKey: 'user_grid_column_id'
    },
    {
       columnDef: 'username',
       columnNameTermKey: 'user_grid_column_username'
    },
    {
       columnDef: 'name',
       columnNameTermKey: 'user_grid_column_name'
    },
    {
       columnDef: 'lastname',
       columnNameTermKey: 'user_grid_column_lastname'
    },
    {
       columnDef: 'email',
       columnNameTermKey: 'user_grid_column_email'
    },
    {
       columnDef: 'profile',
       columnNameTermKey: 'user_grid_column_profile'
    }
  ],
  displayedColumns: [
      'username',
      'name',
      'lastname',
      'email',
      'profile'
  ],
  actions: [
     {
        icon: 'vpn_key',
        dialog: {
           messageTermKey: 'user_grid_action_reset_password_message'
        },
        apiUrl: environment.apiHost + '/user/resetpassword'
     }
  ]
};
