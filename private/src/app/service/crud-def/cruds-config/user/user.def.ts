import { CrudDef } from 'src/app/crud/crud-def';
import { environment } from 'src/environments/environment';
import { USER_CREATE_FORM_FIELDS_DEF } from './form/user.create.form.fields.def';
import { USER_UPDATE_FORM_FIELDS_DEF } from './form/user.update.form.fields.def';
import { USER_GRID_DEF } from './grid/user.grid.def';

// Definicion de un template crud(Create,Read,Update and Delete)
export const USER_DEF: CrudDef = { 
    key: 'user',
    titleTermKey: 'user_title',
    grid: USER_GRID_DEF,
    securityProfile: 'ADMIN',
    forms: {
        create: {
            fields: USER_CREATE_FORM_FIELDS_DEF,
        },
        update : {
            fields: USER_UPDATE_FORM_FIELDS_DEF
        }
    },
    apiUrl: environment.apiHost + '/user'
};
