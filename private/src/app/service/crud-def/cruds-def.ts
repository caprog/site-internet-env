import { CrudDef } from 'src/app/crud/crud-def';
import { COMPANY_DEF } from './cruds-config/company/company.def';
import { EVENT_DEF } from './cruds-config/event/event.def';
import { OURHISTORY_DEF } from './cruds-config/our_history/ourhistory.def';
import { PRIVACYPOLICY_DEF } from './cruds-config/privacy_policy/privacypolicy.def';
import { USER_DEF } from './cruds-config/user/user.def';

export const CRUDS_DEF: CrudDef[] = [
    COMPANY_DEF,
    EVENT_DEF,
    OURHISTORY_DEF,
    USER_DEF,
    PRIVACYPOLICY_DEF
]