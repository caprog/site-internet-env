import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ControlTypeEnum, FieldDef } from 'src/app/field/field-def';
import { environment } from 'src/environments/environment';
import { I18nService } from '../i18n/i18n.service';


const ERRORS_CODES = {
    required : 'form_field_required_error',
    minLength: 'form_field_minlength_error',
    maxLength: 'form_field_maxlength_error',
    email : 'form_field_email_error'
}

@Injectable({
  providedIn: 'root'
})
export class FormService {

    constructor(private formBuilder: FormBuilder, 
                    private i18nService: I18nService) {}

    public buildForm(fields:FieldDef[]): FormGroup{
        const options = fields.reduce((opts, f:FieldDef) =>{
            const prop:any = {};
            prop.value = f.value ? f.value : null;
            prop.disabled = f.disabled ? true : false;
            const fieldOpts = [];
            if(f.minLength)
                fieldOpts.push(Validators.min(f.minLength));
            if(f.maxLength)
                fieldOpts.push(Validators.max(f.maxLength));
            if(f.controlType === ControlTypeEnum.email)
                fieldOpts.push(this.validateEmail(environment.emailRegex));
            if(f.required)
                fieldOpts.push(Validators.required);
            opts[f.key] = [prop, fieldOpts];
            return opts;
        }, {})
        return this.formBuilder.group(options);
    }

    validateEmail(pattern){
        return (control: FormControl) => {
            const isEmpty = str => str === undefined || str === null || str === '';
            return isEmpty(control.value) || pattern.test(control.value) ? null : {
                email: {
                    valid: false
                }
            }
        };
    }

    getFieldErrorMessages(form:FormGroup, fieldKey:string, fields: FieldDef[]): string{
        const controlErrors:ValidationErrors = form.get(fieldKey).errors;
        const field = fields?.find(f => f.key === fieldKey);
        return Object.keys(controlErrors).map(keyError => {
            const controlError = controlErrors[keyError];
            let message = this.i18nService.translate(ERRORS_CODES[keyError]);
            if(field) {
                if(keyError === 'minLength')
                    message.replace('{0}', field.minLength)
                if(keyError === 'maxLength')
                    message.replace('{0}', field.maxLength)
            }

            return message;
        }).join('\n');
    }

    showError(updateForm: FormGroup, fieldKey){
        return updateForm.controls[fieldKey].invalid;
    }
}

