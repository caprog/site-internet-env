import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
export class HttpRequest {
  public url:string;
  public method: string;
}
export class HttpListener{
  public executeAfterSuccess: (httpRequest:HttpRequest) => void;
}
@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private observers: HttpListener[] = [];
  constructor(private http: HttpClient) { }
  
  public subscribe(observer: HttpListener){
    this.observers.push(observer);
  }

  public unsubscribe(observer: HttpListener){
    this.observers.push(observer);
  }

  public get<R>(url: string, params: {[key:string]:any} = null): Observable<R>{
    if(params == null || params == undefined)
      return this.http.get<R>(url);
    else
      return this.http.get<R>(url, {params});
  }

  public post<P,R>(url: string, data:P): Observable<R>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'})
    };
    return this.http.post<R>(url, data, httpOptions);
  }

  public put<P,R>(url: string, data:P): Observable<R>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'})
    };
    return new Observable((subs) =>{
      this.http.put<R>(url, data, httpOptions).subscribe(
        r => {
          subs.next(r);
          this.observers.forEach(obs => obs.executeAfterSuccess({
            url: url,
            method: 'post'
          }));
        },
        e => {
          subs.error(e);
        })
    });
  }

  public delete<R>(url: string, id:number): Observable<R>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'})
    };
    return this.http.delete<R>(url + '/' + id, httpOptions);
  }
}
