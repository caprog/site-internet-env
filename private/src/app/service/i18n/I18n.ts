export enum I18nLangEnum {
    ES = 'es',
    FR = 'fr',
    EN = 'en'
}
export class I18n {
    public key: string;
    public lang: 'es' | 'fr' | 'en';
    public terms: any;
}