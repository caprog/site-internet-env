import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { I18n, I18nLangEnum } from './i18n';

@Injectable({
  providedIn: 'root'
})
export class I18nService {
  private i18nCurrentLang: I18n;
  private loaded: boolean;
  constructor(private httpClient: HttpClient) { 
    this.setCurrentLang(environment.defaultLang);
  }

  private subscribers = [];

  notifyAllSubscribers(){
    this.subscribers.forEach(s => s());
  }

  on18nLoaded(subscriber){
    if(this.loaded)
      subscriber()
    else
      this.subscribers.push(subscriber);
  }

  setCurrentLang(lang: I18nLangEnum){
    this.loaded = false;
    this.httpClient.get(environment.i18nFile + "?random", {responseType: 'text'})
      .subscribe((data:any) => {
        const lines = data.split('\n');
        const idxLang = lines[0].split(';').reduce((pos, col, idx) => col.trim() === lang ? pos = idx: pos) - 1;
        const terms = lines.slice(1).reduce((obj, line) => {
          const columns = line.split(';');
          const term = columns[0];
          const langColumns = columns.slice(1);
          const definition = langColumns[idxLang];
          obj[term ? term.trim() : term] = definition ? definition.trim() : definition;
          return obj;
        }, {})
        this.i18nCurrentLang = {
          key: 'GENERAL',
          lang,
          terms
        }
        this.loaded = true;
        this.notifyAllSubscribers();
      });
  }

  translate(termKey: string){
    const termValue = this.i18nCurrentLang?.terms[termKey];
    return termValue ? termValue : termKey;
  }
}
