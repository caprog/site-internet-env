import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private isInit: boolean;
  constructor() {}

  show(message: string = null): void {
    //@ts-ignore
    const _$ = $;
    if(message == null)
      _$('#loaderMessage').html('');

    _$('#loaderMessage').html(message);
    _$('#loaderPage').stop().fadeIn();
  }

  hide(): void {
    //@ts-ignore
    $('#loaderPage').fadeOut("slow")
  }
}
