import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { I18nService } from '../i18n/i18n.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public constructor(private snackBar: MatSnackBar, private i18nService: I18nService){}
  showSuccessMessage(message: string) {
    this.snackBar.open(message, this.i18nService.translate('notification_success_message_close_button_label'), {
      duration: 2000,
    }); 
  }

  showErrorMessage(message: string) {
    this.snackBar.open(message, this.i18nService.translate('notification_error_message_close_button_label'), {
      duration: 2000,
    }); 
  }

  showServerError() {
    this.showErrorMessage(this.i18nService.translate('notification_server_error'));
  }
}
