import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable, Subscriber } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(obs => {
        this.authService.hasToken()
          .subscribe(hasToken => {
            if(hasToken)
              this.redirectUser(obs, state);
            else {
              this.router.navigate([environment.paths.login]);
              obs.next(true)
            }
          });
      });
  }

  private redirectUser(obs: Subscriber<boolean>, state: RouterStateSnapshot){
      if(state.url === '/') {
        this.authService
              .getUser()
              .subscribe(user => {
                if(user.profile === environment.profiles.admin)
                  this.router.navigate([environment.paths.user]);
                else if(user.profile === environment.profiles.writer)
                  this.router.navigate([environment.paths.company]);
                obs.next(true);
              },
              err => obs.next(true))
      }else {
        obs.next(true)
      }
  }
}
