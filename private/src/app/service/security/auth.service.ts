import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../http/ApiResponse';
import { HttpService } from '../http/http.service';
import { User } from './user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtLocalStorage = environment.jwtLocalStorage;
  private userUrl = environment.apiHost + '/user/session';
  private authUrl = environment.apiHost + '/auth';
  
  constructor(private httpService: HttpService) {}
  
  public login(username:string, password:string): Observable<boolean> {
    const user:User = {username,  password};
    return new Observable<boolean>(obs => {
      this.httpService.post<User, ApiResponse>(this.authUrl, user)
          .subscribe(            (a:ApiResponse) => {
              const token:string = a.data.token;
              this.setToken(token);
              obs.next(true);
            },
            err => obs.error(err));
    });
  }

  public getUser(): Observable<User>{
    return new Observable<User>(obs => {
      this.httpService.get<ApiResponse>(this.userUrl)
          .subscribe(            
            (a:ApiResponse) => {
              const user:User = a.data;
              obs.next(user);
            },
            err => obs.error(err));
    });
  }

  public logout(): Observable<boolean>{
    return new Observable(obs => {
      localStorage.removeItem(this.jwtLocalStorage);
      obs.next(true);
    });
  }

  public hasToken(): Observable<boolean>{
    return new Observable(obs => {
      const token = this.getToken();
      obs.next(token !== undefined && token !== null);
    });
  }

  public getToken(): string {
    return localStorage.getItem(this.jwtLocalStorage);
  }

  private setToken(token: string): void {
    localStorage.setItem(this.jwtLocalStorage, token);
  }
}

