import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { LoaderService } from '../loader/loader.service';

@Injectable({
  providedIn: 'root'
})
export class JwkInterceptor implements HttpInterceptor {

  constructor(public authService: AuthService,
              public loaderService: LoaderService,
              private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      this.authService.hasToken()
        .subscribe(hasToken => {
          if (hasToken) {
            request = request.clone({
                headers: request.headers.set('X-Bearer-Token', this.authService.getToken())
            });
          }
      });
      return next.handle(request).pipe(
        catchError((error: HttpErrorResponse) => {
          if(error.status === 401){
            this.authService.logout()
                  .subscribe(r => {
                    this.forceRedirect('/')
                  });
          }
          this.loaderService.hide();
          return throwError(error);
        })
      );
  }

  private forceRedirect(url: string){
    this.router.navigateByUrl('/refresh', {skipLocationChange: true})
      .then(()=> this.router.navigate([url]));
  }

}
