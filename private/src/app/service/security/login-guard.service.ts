import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable, Subscriber } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable()
export class LoginGuardService implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(obs => {
        this.authService.hasToken()
          .subscribe(hasToken => {
            if(hasToken)
              this.router.navigate(['/']);
            else {
              obs.next(true)
            }
          });
      });
  }
}
