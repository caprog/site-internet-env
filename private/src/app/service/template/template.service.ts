import { Injectable } from '@angular/core';
export class Template{
  public title: string;
}
export interface TemplateObserver{
  notify(obj: Template):void;
}
@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  private template: Template = { title: ''};
  private observers: TemplateObserver[] = [];
  constructor() { 

  }

  subscribeChange(observer: TemplateObserver){
    this.observers.push(observer);
    observer.notify(this.template);
  }

  unsubscribeChange(observer: TemplateObserver){
    this.observers = this.observers.filter(obs => obs != observer);
  }

  changeTitle(title: string){
    this.template.title = title;
    this.observers.forEach(obs => obs.notify(this.template));
  }
}
