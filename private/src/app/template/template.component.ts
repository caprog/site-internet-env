import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudDef } from '../crud/crud-def';
import { CrudDefService } from '../service/crud-def/crud-def.service';
import { I18nService } from '../service/i18n/i18n.service';
import { LoaderService } from '../service/loader/loader.service';
import { NotificationService } from '../service/notification/notification.service';
import { AuthService } from '../service/security/auth.service';
import { Template, TemplateService } from '../service/template/template.service';
import { delay } from 'rxjs/internal/operators';
import { User } from '../service/security/user';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {
  
  public template: Template;
  public rootSite = environment.rootSite;
  public privacyPolicyUrl = environment.privacyPolicyUrl;
  public pages: {title:string, url: string } [];
  private user: User;
  constructor(private router: Router,
              private loaderService: LoaderService,
              private notificationService: NotificationService,
              private templateService: TemplateService,
              private crudDefService: CrudDefService,
              private authService: AuthService,
              private i18nService: I18nService) {
    this.templateService.subscribeChange({
      notify: (obj:Template) => this.template = obj
    })
  }

  redirect(url:string){
    this.forceRedirect(url);
  }

  private forceRedirect(url: string){
    this.router.navigateByUrl('/refresh', {skipLocationChange: true})
      .then(()=> this.router.navigate([url]));
  }

  ngOnInit(): void {
    this.authService.getUser()
    .subscribe(
      user => {
        this.user = user;
        this.crudDefService.findAllCrudDef()
          .subscribe((cruds:CrudDef[]) => {
            this.pages = cruds.filter(crud => crud.securityProfile === this.user.profile)
              .map(crud => {
                return {
                  title: this.i18nService.translate(crud.titleTermKey),
                  url: '/' + crud.key
                }
              });
        });

      },
      err => this.notificationService.showServerError()
    )


      
  }

  signOut(){
    this.loaderService.show(this.translate('user_signing_out'));
      this.authService.logout()
        .pipe(delay(1000))
        .subscribe(
          ok => {
            this.forceRedirect('/')
            this.loaderService.hide();
          },
          err => {
            this.notificationService.showServerError();
            this.loaderService.hide();
          });
  }

  getUserInfo(){
    if(this.user === undefined)
      return '';

    let profile;
    if(this.user.profile === 'ADMIN')
      profile = this.translate('header_user_admin_type');
    else if(this.user.profile === 'WRITER')
      profile = this.translate('header_user_writer_type');
    
    return `${profile}: ${this.user.username}`;
  }

  translate(term:string) {
    return this.i18nService.translate(term);
  }
}
