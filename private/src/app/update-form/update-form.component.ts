import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CrudDef } from '../crud/crud-def';
import { FormUtils } from '../form/form-utils';
import { FormService } from '../service/form/form.service';
import { ApiResponse } from '../service/http/ApiResponse';
import { HttpService } from '../service/http/http.service';
import { I18nService } from '../service/i18n/i18n.service';
import { LoaderService } from '../service/loader/loader.service';
import { NotificationService } from '../service/notification/notification.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.scss']
})
export class UpdateFormComponent{

  updateForm: FormGroup;
  private submitting: boolean;
  @Input()
  crudDef: CrudDef;
  private _idData: number;
  private entityId: number;
  @Input() set idData(valeur: number) {
    this._idData = valeur;
    this.reload();
  }
  get idData(): number{
    return this._idData;
  }

  constructor(
      private i18nService: I18nService,
      private httpService: HttpService,
      private formService: FormService,
      private notificationService: NotificationService,
      private loaderService: LoaderService) {}
      
  reload(){
    console.log('reload', this.idData);
    this.updateForm = this.formService.buildForm(this.crudDef.forms.update.fields);
    this.loaderService.show(this.translate('crud_update_form_getting_data'));
    const url = this.crudDef.forms.update.apiGet ? this.crudDef.forms.update.apiGet: this.crudDef.apiUrl;
    const data = this.idData ? {id: this.idData} : undefined;
    this.httpService.get(url, data)
      .subscribe(
        (r: ApiResponse) => {
          this.entityId = r.data.id;
          this.updateForm.patchValue(r.data);
          this.loaderService.hide();
        },
        err => {
          this.notificationService.showServerError();
          this.loaderService.hide();
          console.error(err);
        });
  }
  submit(): void{
    if(this.submitting)
      return;
    
    this.submitting = true;
    if(this.updateForm.valid){
      this.loaderService.show(this.translate('crud_update_form_updating_data'));
      const data:any = FormUtils.getDataControls<any>(this.updateForm.controls);
      data.id = this.entityId;
      this.httpService
        .put(this.crudDef.apiUrl, data)
        .subscribe((r: ApiResponse) => {
          this.submitting = false;
          this.loaderService.hide();
          this.notificationService.showSuccessMessage(this.translate('crud_update_form_notify_success_message'));
        },
        err => {
          this.submitting = false;
          this.notificationService.showServerError();
          this.loaderService.hide();
          console.error(err);
        });
    }
  }

  translate(termKey: string): string{
    return this.i18nService.translate(termKey);
  }

}