// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { I18nLangEnum } from 'src/app/service/i18n/i18n';

export const environment = {
  production: true,
  defaultLang: I18nLangEnum.FR,
  defaultDateLang: "fr-FR",
  rootSite: "/site-internet-env/",
  privacyPolicyUrl: "/site-internet-env/legal",
  apiHost: '/site-internet-env/api',
  i18nFile: '/site-internet-env/private-access/res/i18n.csv',
  userLocalStorage: 'user_data',
  jwtLocalStorage: 'jwt_data',
  profiles: {
    admin: 'ADMIN',
    writer: 'WRITER'
  },
  paths: {
    login: '/login',
    company: '/company',
    user: '/user'
  },
  datetimeFormat: 'DD/MM/YYYY HH:mm',
  emailRegex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
};

window.console.log = () => { };
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.