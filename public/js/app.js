function App() {
    this.start = () => {
        // For validations form
        const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        const isValidEmail = (email) => emailRegex.test(email)
        const isMinLengthRespected = (value, length) => value.length >= length;
        const isMaxLengthRespected = (value, length) => value.length <= length;
        const isEmpty = (value) => value == undefined || value == null || value.trim() === ''
        const hasErrorFullname = (fullname) => isEmpty(fullname)
        const hasErrorEmail = (email) => isEmpty(email) || !isValidEmail(email)
        const hasErrorSubject = (subject) => isEmpty(subject)
        const hasErrorMessage = (message) => isEmpty(message) || !isMaxLengthRespected(message, 350) || !isMinLengthRespected(message, 10)
        
        // Api Rest Contact
        const apiContact = '/site-internet-env/api/contact'
        const sendContactFormData = async (formData) => {
            return new Promise(async (res, rej) => {
                try {
                    const response = await fetch(apiContact, {
                        method: 'POST', // *GET, POST, PUT, DELETE, etc.
                        mode: 'cors', // no-cors, *cors, same-origin
                        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                        credentials: 'same-origin', // include, *same-origin, omit
                        headers: {
                          'Content-Type': 'application/json'
                        },
                        redirect: 'follow', // manual, *follow, error
                        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                        body: JSON.stringify(formData) // body data type must match "Content-Type" header
                      })
                      if(response.ok)
                          res(await response.json())
                      else
                          rej(response)
                }catch(err){
                    rej(err)
                }
            })
        }
        // Context Vue JS
        const ctx = new Vue({
            el: '#app',
            data: {
                isMenuOpen: false,
                hasTopLogo: true,
                showCookieMessage: !isCookieAccepted(),
                contactFormData: {},
                contactFormErrors: {},
                contactFormSuccessfulSubmit: false,
                contactFormErrorSubmit: false
            },
            methods: {
                toggleMenu(event) {
                    event.preventDefault()
                    this.isMenuOpen = !this.isMenuOpen
                },

                submitContactForm(event) {
                    event.preventDefault()

                    this.contactFormErrors = {}
                    this.contactFormSuccessfulSubmit = false
                    this.contactFormErrorSubmit = false

                    const formData = this.contactFormData
                    let hasError = false
                    if(hasErrorFullname(formData.fullname))
                        this.contactFormErrors.fullname = hasError = true

                    if(hasErrorEmail(formData.email))
                        this.contactFormErrors.email = hasError = true

                    if(hasErrorSubject(formData.subject))
                        this.contactFormErrors.subject = hasError = true
                    
                    if(hasErrorMessage(formData.message))
                        this.contactFormErrors.message = hasError = true
                    
                    if(!hasError)
                        sendContactFormData(formData).then(r => {
                            this.contactFormSuccessfulSubmit = true
                        }).catch(err => {
                            this.contactFormErrorSubmit = true
                        })
                },

                acceptCookies(event) {
                    enableCookies()
                    this.showCookieMessage = false
                    this.$forceUpdate()
                    event.preventDefault()
                },

                closeCookieMessage() {
                    this.showCookieMessage = false
                }
            },
        })
    }
}
        