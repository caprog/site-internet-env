const cookie_setter = document.__lookupSetter__("cookie");
const cookie_getter = document.__lookupGetter__("cookie");

function disableCookies(){
    if(!document.__defineGetter__) {
        Object.defineProperty(document, 'cookie', {
            get: function(){return ''},
            set: function(){return true},
        });
    } else {
        document.__defineGetter__("cookie", function() { return '';} );
        document.__defineSetter__("cookie", function() {} );
    }
}

function enableCookies(){
    document.__defineGetter__("cookie", cookie_getter);
    document.__defineSetter__("cookie", cookie_setter);
    localStorage.setItem('acceptCookies', 'true');
}

function isCookieAccepted(){
    return localStorage.getItem('acceptCookies') === 'true';
}

if(!isCookieAccepted()){
    disableCookies();
}