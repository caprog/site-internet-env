/*
* Redirect to https in server caprog.com
* Remove in with a real hosting 
*/  
const isHost = window.location.host === 'iglesianuevavida.com';
const isProtocolHttps = window.location.protocol === 'https:';
if(isHost && !isProtocolHttps){
    window.location.href = window.location.href.replace('http:', 'https:');
}